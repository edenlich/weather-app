import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class IconRegistryModule { 
  constructor(private domSanitizer: DomSanitizer, public matIconRegistry: MatIconRegistry) {
    this.registerIcons();
  }

  registerIcons(): void {
    const icons = [
      {
        name: 'SEARCH',
        path: 'assets/icons/search-icon.svg',
      },
      {
        name: 'FAVORITE',
        path: 'assets/icons/favorite-icon.svg',
      },
      {
        name: 'EMPTY_FAVORITE',
        path: 'assets/icons/empty-favorite-icon.svg',
      },
      {
        name: 'CIRCLE',
        path: 'assets/icons/circle-icon.svg',
      }
    ];

    icons.forEach((icon) => {
      this.matIconRegistry.addSvgIcon(
        icon.name,
        this.domSanitizer.bypassSecurityTrustResourceUrl(icon.path),
      );
    });
  }
}
