import { Location } from "src/app/shared/models/location.model";

export const defaultLocation: Location = {
  'Version': 1,
  'Key': '298198',
  'Type': 'City',
  'Rank': 20,
  'LocalizedName': 'Belgrade',
  'Country': {
    'ID': 'RS',
    'LocalizedName': 'Serbia'
  },
  'AdministrativeArea': {
    'ID': '00',
    'LocalizedName': 'Belgrade',
  },
};