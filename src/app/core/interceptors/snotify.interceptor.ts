import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
  HttpResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { SnotifyService } from 'ng-snotify';
import { catchError, map } from 'rxjs/operators';

@Injectable()
export class SnotifyInterceptor implements HttpInterceptor {

  constructor(private snotifyService: SnotifyService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(request).pipe(
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          this.snotifyService.success('The Operation Completed Successfully');
        }
        return event;
      }),
      catchError((error: HttpErrorResponse) => {
        this.snotifyService.error('The Operation Failed');
        return throwError(error);
      }),
    );
  }
}
