import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WeatherRoutingModule } from './weather-routing.module';
import { SearchPage } from './pages/search/search.page';
import { FavoritesPage } from './pages/favorites/favorites.page';
import { SharedModule } from 'src/app/shared/shared.module';
import { ForecastListComponent } from './components/forecast-list/forecast-list.component';
import { ForecastListItemComponent } from './components/forecast-list-item/forecast-list-item.component';
import { CurrentWeatherListComponent } from './components/current-weather-list/current-weather-list.component';
import { CurrentWeatherListItemComponent } from './components/current-weather-list-item/current-weather-list-item.component';


@NgModule({
  declarations: [
    SearchPage, 
    FavoritesPage, 
    ForecastListComponent, 
    ForecastListItemComponent, 
    CurrentWeatherListComponent, 
    CurrentWeatherListItemComponent
  ],
  imports: [
    CommonModule,
    WeatherRoutingModule,
    SharedModule,
  ]
})
export class WeatherModule { }
