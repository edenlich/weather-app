import { Component, OnInit } from '@angular/core';
import { WeatherService } from 'src/app/core/http/weather.service';
import { FavoritesService } from 'src/app/core/services/favorites.service';
import { CurrentWeather } from 'src/app/shared/models/currentWeather.model';
import { Location } from 'src/app/shared/models/location.model';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.page.html',
  styleUrls: ['./favorites.page.scss']
})
export class FavoritesPage implements OnInit {
  favorites: Location[] = [];

  favoritesWithWeather: { location: Location, currentWeather: CurrentWeather }[] = [];

  constructor(private favoritesService: FavoritesService, private weatherService: WeatherService) { }

  ngOnInit(): void {
    this.subscribeToTemperatureUnitChange();
    this.loadFavorites();
  }

  subscribeToTemperatureUnitChange() {
    this.weatherService.temperatureUnitChanged.subscribe(() => this.loadCurrentWeather());
  }
  
  loadFavorites(): void {
    this.favorites = this.favoritesService.getFavorites();
    this.loadCurrentWeather();
  }

  loadCurrentWeather(): void {
    this.favoritesWithWeather = [];

    for(let i = 0; i < this.favorites.length; i++) {
      this.weatherService.getCurrentWeather(this.favorites[i].Key).subscribe((currentWeather) => {
            this.favoritesWithWeather.push({ location: this.favorites[i], currentWeather: currentWeather[0] });
          });
    };
  }
}
