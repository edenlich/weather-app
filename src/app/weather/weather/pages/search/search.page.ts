import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { defaultLocation } from 'src/app/core/defaults/location.default';
import { LocationService } from 'src/app/core/http/location.service';
import { WeatherService } from 'src/app/core/http/weather.service';
import { FavoritesService } from 'src/app/core/services/favorites.service';
import { DailyForecast, Forecast } from 'src/app/shared/models/forecast.model';
import { Location } from 'src/app/shared/models/location.model';
import { map, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss']
})
export class SearchPage implements OnInit {
  searchInput: FormControl = new FormControl();

  location: Location = defaultLocation;

  isFavorite: boolean;

  locationForecast: Forecast;

  dailyForecasts: DailyForecast[];

  filteredOptions;

  constructor(
    private weatherService: WeatherService, 
    private locationService: LocationService,
    private favoritesService: FavoritesService,
    private route: ActivatedRoute,
  ) {
    this.filteredOptions = this.searchInput.valueChanges
        .pipe(
          debounceTime(400),
          distinctUntilChanged(),
          switchMap(value => {
            return this.filterAutocompleteOptions(value || '')
          })       
        );
   }

   filterAutocompleteOptions(value: string): Observable<Location[]> {
    return this.locationService.getAutocompleteLocation(this.searchInput.value)
     .pipe(
       map(response => response.filter(option => { 
         return option.LocalizedName.toLowerCase().indexOf(value.toLowerCase()) === 0
       })),
     );
   }  

  ngOnInit(): void {
    let locationKey;
    this.route.paramMap.subscribe((params) => {
      locationKey = params.get('locationKey');
      this.loadLocation(locationKey);
    });

    this.subscribeToTemperatureUnitChange();
    
    if (!locationKey) {
      this.location = defaultLocation;
      this.loadForecast();
      this.isLocationInFavorites();
    } 
  }

  subscribeToTemperatureUnitChange() {
    this.weatherService.temperatureUnitChanged.subscribe(() => this.loadForecast());
  }

  loadForecast(): void {
    this.weatherService.getForecast(this.location.Key).subscribe((forecast: Forecast) => {
      this.locationForecast = forecast;
      this.dailyForecasts = forecast.DailyForecasts;
    });
  }

  loadLocation(key?: string): void {
    const locationKey = key ?? (this.searchInput.value ? this.searchInput.value.Key : '');

    this.locationService.getLocationByKey(locationKey).subscribe((location: Location) => {
      this.location = location;
      this.loadForecast();
      this.isLocationInFavorites();
    })
  }

  isLocationInFavorites(): void {
    const favorites = this.favoritesService.getFavorites();
    this.isFavorite = favorites.some((favorite: Location) => favorite.Key === this.location.Key)
  }

  addToFavorites(): void {
    this.favoritesService.addToFavorites(this.location);
    this.isFavorite = true;
  }

  removeFromFavorites(): void {
    this.favoritesService.removeFromFavorites(this.location.Key);
    this.isFavorite = false;
  }

  displayAutocompleteResult(location: Location) {
    return location ? `${location.LocalizedName}, ${location.Country.LocalizedName}` : '';
  }
}

