import { Component, Input, OnInit } from '@angular/core';
import { DailyForecast } from 'src/app/shared/models/forecast.model';

@Component({
  selector: 'app-forecast-list',
  templateUrl: './forecast-list.component.html',
  styleUrls: ['./forecast-list.component.scss']
})
export class ForecastListComponent implements OnInit {
  @Input() forecastList: DailyForecast[] = [];

  constructor() { }

  ngOnInit(): void {
  }

}
