import { Component, Input, OnInit } from '@angular/core';
import { CurrentWeather } from 'src/app/shared/models/currentWeather.model';
import { Location } from 'src/app/shared/models/location.model';

@Component({
  selector: 'app-current-weather-list',
  templateUrl: './current-weather-list.component.html',
  styleUrls: ['./current-weather-list.component.scss']
})
export class CurrentWeatherListComponent implements OnInit {
  @Input() favoritesWithWeather: { location: Location, currentWeather: CurrentWeather }[];

  constructor() { }

  ngOnInit(): void {
  }

}
