import { Component, Input, OnInit } from '@angular/core';
import { WeatherService } from 'src/app/core/http/weather.service';
import { FavoritesService } from 'src/app/core/services/favorites.service';
import { CurrentWeather } from 'src/app/shared/models/currentWeather.model';
import { Location } from 'src/app/shared/models/location.model';

@Component({
  selector: 'app-current-weather-list-item',
  templateUrl: './current-weather-list-item.component.html',
  styleUrls: ['./current-weather-list-item.component.scss']
})
export class CurrentWeatherListItemComponent implements OnInit {
  @Input() favorite: { location: Location, currentWeather: CurrentWeather };

  temperatureValue;

  isFavorite: boolean;

  constructor(private favoritesService: FavoritesService, private weatherService: WeatherService) { }

  ngOnInit(): void {
    this.isLocationInFavorites();

    this.temperatureValue = this.weatherService.isMetric 
    ? this.favorite.currentWeather.Temperature.Metric.Value
    : this.favorite.currentWeather.Temperature.Imperial.Value
  }

  isLocationInFavorites(): void {
    const favorites = this.favoritesService.getFavorites();
    this.isFavorite = favorites.some((favorite: Location) => favorite.Key === this.favorite.location.Key)
  }

  addToFavorites(): void {
    this.favoritesService.addToFavorites(this.favorite.location);
    this.isFavorite = true;
  }

  removeFromFavorites(): void {
    this.favoritesService.removeFromFavorites(this.favorite.location.Key);
    this.isFavorite = false;
  }
}
