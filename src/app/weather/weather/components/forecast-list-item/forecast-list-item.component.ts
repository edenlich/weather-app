import { Component, Input, OnInit } from '@angular/core';
import { DailyForecast } from 'src/app/shared/models/forecast.model';

@Component({
  selector: 'app-forecast-list-item',
  templateUrl: './forecast-list-item.component.html',
  styleUrls: ['./forecast-list-item.component.scss']
})
export class ForecastListItemComponent implements OnInit {
  @Input() forecast: DailyForecast;

  constructor() { }

  ngOnInit(): void {
  }

}
